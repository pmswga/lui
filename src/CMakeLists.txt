cmake_minimum_required(VERSION 3.5)

project(lui-src LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(lui-src 
    main.cpp
    app.h
    app.cpp
    frontend/lexer.h
    frontend/lexer.cpp
    frontend/token.h
    frontend/syntaxer.h
    frontend/syntaxer.cpp
    )
