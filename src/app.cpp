#include "app.h"

App::App()
    : argc(0), argv(nullptr)
{
    
}

App::App(unsigned int argc, char **argv)
    : argc(argc), argv(argv)
{
    
}

App::~App()
{
    
}

void App::run()
{
    
}
