#include "lexer.h"

using namespace lui::translator::frontend;

Lexer::Lexer()
    : tokens(), code("")
{
    
}

Lexer::Lexer(const Lexer &l)
    : tokens(), code("")
{
    this->code = std::move(l.code);
    this->tokens = {};
}

Lexer::~Lexer()
{
    if (tokens.size() > 0) {
        for (Token<std::string> *t : tokens) {
            if (t != nullptr) {
                delete t;
            }
        }
    }
}

void Lexer::setCode(const std::string code)
{
    this->code = code;
    this->code.append(" ");
}

TokenList Lexer::parse()
{
    //auto state(0);
    std::string lexem;
    auto isQuotes(false);
    
    for (auto c = this->code.begin(); c != code.end(); c++)
    {
        
        if (*c == CHARACTER_TABLE::C_QUOTE) {
            isQuotes = !isQuotes ? true : false;
        }
        
        if (!isQuotes && *c == CHARACTER_TABLE::C_SPACE)
        {
            lexem = this->lstrip(lexem);
            if (isComponentName(lexem)) {
                this->tokens.push_back(new Token<std::string>(TokenType::COMPONENT, lexem));
            } else if (isPropertyName(lexem)) {
                this->tokens.push_back(new Token<std::string>(TokenType::PROPERTY_NAME, lexem));
            } else if (isPropertyStringValue(lexem)) {
                this->tokens.push_back(new Token<std::string>(TokenType::PROPERTY_STRING_VALUE, lexem));
            } else if (isPropertyNumberValue(lexem)) {
                this->tokens.push_back(new Token<std::string>(TokenType::PROPERTY_NUMBER_VALUE, lexem));
            }
            
            lexem.clear();
        }
        
        lexem += *c;
    }
    
    
    /*
    for (auto c = this->code.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (isalpha(*c) || isalnum(*c)) {
                    state = START;
                    token += *c;
                    c++;
                } else if (isspace(*c)) {
                    state = 1;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (*c == ' ') {
                    state = 2;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (isComponentName(token)) {
                    //create token;
                    state = 3;
                    token = *c;
                } else {
                    state = ERROR;
                }
            } break;
            case 3:
            {
                if (isOBrace(token)) {
                    //create token;
                }
            } break;
            case 4:
            {
                //hmmm
            } break;
        }
        
    }
    
    */
    return this->tokens;
}

bool Lexer::checkBraces() const
{
    auto braceCount(0);
    
    for (auto c : this->code)
    {
        switch (c)
        {
        case CHARACTER_TABLE::C_OBRACE: braceCount++; break;
        case CHARACTER_TABLE::C_CBRACE: braceCount--; break;
        }
    }
    
    return braceCount == 0;
}

bool Lexer::isOBrace(std::string obrace) const
{
    auto state(0);
    
    for (auto c = obrace.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (ispunct(*c)) {
                    state = 1;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (*c == CHARACTER_TABLE::C_OBRACE) {
                    state = 2;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (c == obrace.end()) {
                    state = FINISH;
                } else {
                    state = ERROR;
                }
            } break;
            case ERROR: return false;
        }
    }
    
    return state == FINISH;
}

bool Lexer::isCBrace(std::string cbrace) const
{
    auto state(0);
    
    for (auto c = cbrace.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (ispunct(*c)) {
                    state = 1;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (*c == CHARACTER_TABLE::C_CBRACE) {
                    state = 2;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (c == cbrace.end()) {
                    state = FINISH;
                } else {
                    state = ERROR;
                }
            } break;
            case ERROR: return false;
        }
    }
    
    return state == FINISH;
}

bool Lexer::isComponentName(std::string componentName) const
{
    auto state(0);
    
    for (auto c = componentName.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (isupper(*c)) {
                    state = 1;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (isalpha(*c) && islower(*c)) {
                    state = 1;
                    c++;
                } else if (iscntrl(*c)) {
                    state = 2;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (c == componentName.end()) {
                    state = FINISH;
                } else {
                    state = ERROR;
                }
            } break;
            case ERROR: return false;
        }
    }
    
    return state == FINISH;
}

bool Lexer::isPropertyName(std::string propertyName) const
{
    auto state(0);
    
    for (auto c = propertyName.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (isalpha(*c)) {
                    state = 0;
                    c++;
                } else if (ispunct(*c)) {
                    state = 1;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (*c == ':') {
                    state = 2;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (c == propertyName.end()) {
                    state = FINISH;
                } else {
                    state = ERROR;
                }
            } break;
            case ERROR: return false;
        }
    }
    
    return state == FINISH;
}

bool Lexer::isPropertyNumberValue(std::string propertyValue) const
{
    auto state(0);
    std::string value;
    
    for (auto c = propertyValue.begin(); state != FINISH;)
    {
        switch (state)
        {
            case START:
            {
                if (*c == CHARACTER_TABLE::C_MINUS || isdigit(*c)) {
                    state = 1;
                    value += *c;
                    c++;
                } else {
                    state = ERROR;
                }
            } break;
            case 1:
            {
                if (isdigit(*c)) {
                    state = 1;
                    value += *c;
                    c++;
                } else if (iscntrl(*c)) {
                    state = 2;
                } else {
                    state = ERROR;
                }
            } break;
            case 2:
            {
                if (std::atof(value.c_str()) != 0) {
                    state = 3;
                } else {
                    state = ERROR;
                }
            } break;
            case 3:
            {
                if (c == propertyValue.end()) {
                    state = FINISH;
                } else {
                    state = ERROR;
                }
            } break;
            case ERROR: return false;
        }
    }
    
    return state == FINISH;
}

bool Lexer::isPropertyStringValue(std::string propertyValue) const
{
    
    return true;
}

bool Lexer::isPropertyVarValue(std::string propertyValue) const
{
    
    return true;
}

std::string Lexer::lstrip(std::string str) const
{
    std::string lstr("");
    auto isFirstLetter(false);
    
    for (auto c : str)
    {
        if (!isspace(c)) {
            isFirstLetter = true;
        }
        
        if (isFirstLetter) {
            lstr += c;
        }
    }
    
    return lstr;
}

void Lexer::debug()
{
    std::clog << "=[Token list]=========" << std::endl;
    
    for (auto t : this->tokens)
    {
        std::clog << t->getTypeToString() << " | " << t->getData() << std::endl;
    }
    
    
}
