#ifndef LEXER_H
#define LEXER_H

#include <vector>
#include <string>
#include <cctype>

#include <iostream>
#include <iomanip>
#include "token.h"

namespace lui {

namespace translator {

namespace frontend {

//fix it?
enum CHARACTER_TABLE
{
    C_OBRACE  = '{',
    C_CBRACE  = '}',
    C_NEWLINE = '\n',
    C_SPACE   = ' ',
    C_QUOTE   = '\"',
    C_MINUS   = '-'
};

typedef std::vector<Token<std::string>*> TokenList;

class Lexer
{
    TokenList tokens;
    std::string code;
    enum states {
        START  = 0,
        ERROR  = -1,
        FINISH = -2
    };
    
public:
    Lexer();
    Lexer(const Lexer &l);
    virtual ~Lexer();
    
    void setCode(const std::string code);
    
    TokenList parse();

    bool checkBraces() const;
    bool isOBrace(std::string obrace) const;
    bool isCBrace(std::string cbrace) const;
    bool isComponentName(std::string componentName) const;
    bool isPropertyName(std::string propertyName) const;
    bool isPropertyNumberValue(std::string propertyValue) const;
    bool isPropertyStringValue(std::string propertyValue) const;
    bool isPropertyVarValue(std::string propertyValue) const;


    auto lstrip(std::string str) const -> std::string;
    
    void debug();
    
private:
    
    
};

} // frontend

} // translator

} // Lui


#endif // LEXER_H
