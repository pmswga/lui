#ifndef TOKEN_H
#define TOKEN_H

#include <cstring>
#include <string>

namespace lui {

namespace translator {

namespace frontend {

enum TokenType
{
    EMPTY                 = 0,
    COMPONENT             = 1,
    PROPERTY_NAME         = 2,
    PROPERTY_NUMBER_VALUE = 3,
    PROPERTY_STRING_VALUE = 4,
    PROPERTY_VAR          = 5,
    OBRACE                = 6,
    CBRACE                = 7
};

template <class Lexem>
class Token
{
    TokenType tokenType;
    Lexem tokenData;
    
public:
    Token()
        : tokenType(TokenType::EMPTY)
    {
        
    }
     
    Token(const TokenType token_type)
        : tokenType(token_type)
    {
        
    }
    
    Token(const TokenType token_type, const Lexem token_data)
        : tokenType(token_type), tokenData(token_data)
    {
        
    }
    
    virtual ~Token()
    {
        
    }
    
    void setType(const TokenType tokenType)
    {
        this->tokenType = tokenType;
    }
    
    auto getType() const -> TokenType
    {
        return this->tokenType;
    }
    
    auto getTypeToString() const -> std::string
    {
        switch (this->tokenType)
        {
            case EMPTY: return "Empty"; break;
            case COMPONENT: return "Component"; break;
            case PROPERTY_NAME: return "Property name"; break;
            case PROPERTY_NUMBER_VALUE: return "Property number value"; break;
            case PROPERTY_STRING_VALUE: return "Property var"; break;
            case OBRACE: return "Open brace"; break;
            case CBRACE: return "Close brace"; break;
        }
    }
    
    
    void setData(const Lexem tokenData)
    {
        this->tokenData = tokenData;
    }
    
    auto getData() const -> Lexem
    {
        return this->tokenData;
    }
};

} // frontend

} // translator

} // lui

#endif // TOKEN_H
