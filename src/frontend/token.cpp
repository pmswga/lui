#include "token.h"

using namespace lui::translator::frontend;

template class Token<::std::string>;

template <class Lexem>
Token<Lexem>::Token()
    : token_type(EMPTY)
{
    
}

//template<class Lexem>
//Token<Lexem>::Token(const TokenType token_type, const Lexem token_data)
//    : token_type(token_type), token_data(token_data)
//{
    
//}

template <class Lexem>
Token<Lexem>::Token(const TokenType token_type)
    : token_type(token_type)
{
    
}


template <class Lexem>
Token<Lexem>::~Token<Lexem>()
{
    
}


template <class Lexem>
void Token<Lexem>::setType(const TokenType token_type)
{
    this->token_type = token_type;
}

template <class Lexem>
TokenType Token<Lexem>::getType() const
{
    return this->token_type;
}

