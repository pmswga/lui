#ifndef APP_H
#define APP_H

#include "frontend/lexer.h"


class App
{
    unsigned int argc;
    char **argv;
    
public:
    App();
    App(unsigned int argc, char **argv);
    virtual ~App();
    
    void run();
    
    
};

#endif // APP_H
